@extends('layouts.app')

@section('title')
    Бухгалтерия
@endsection

@section('content')
    <section class="bg-title-page p-b-50 flex-col-c-m"
             style="background-image: url({{ asset('images/heading-pages-01.jpg') }});">
        <h2 class="l-text2 t-center">
            Бухгалтерия за {{ \Carbon\Carbon::now()->format('d/m/Y') }}
        </h2>
    </section>

    <section class="bgwhite p-b-60">
        <div class="container">
            <div class="row">
                <div class="col-md-12 p-t-30">
                    {!! Form::open(['route' => 'accountingStore']) !!}
                    <h4 class="p-b-5">Сумма заказа за сегодня: </h4>
                    <h4 class="p-b-10">
                        <u>{{ number_format(\App\Model\Order::where('user_id', Auth::user()->id)->whereDate('created_at', '=', \Carbon\Carbon::today()->toDateString())->first()->total_price, 0, '', ' ') }}</u>
                        сум</h4>
                    <input type="hidden" name="orderSumForToday"
                           value="{{ \App\Model\Order::where('user_id', Auth::user()->id)->whereDate('created_at', '=', \Carbon\Carbon::today()->toDateString())->first()->total_price }}">

                    <h4 class="m-t-10">Внутренний</h4>
                    <div class="bo4 of-hidden size15 m-b-20">
                        <input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="innerSum" placeholder="Внутренний"
                               required onkeypress="javascript:return isNumber(event)">
                    </div>

                    <h4 class="m-t-10">Инкассация</h4>
                    <div class="bo4 of-hidden size15 m-b-20">
                        <input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="taxSum" placeholder="Инкассация"
                               required onkeypress="javascript:return isNumber(event)">
                    </div>

                    <h4 class="m-t-10">Терминал</h4>
                    <div class="bo4 of-hidden size15 m-b-20">
                        <input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="terminalSum" placeholder="Терминал"
                               required onkeypress="javascript:return isNumber(event)">
                    </div>

                    <div class="w-size25">
                        <!-- Button -->
                        <button class="flex-c-m size2 bg1 bo-rad-23 hov1 m-text3 trans-0-4">
                            Отправить
                        </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript">
        function isNumber(evt) {
            var keyCode = evt.keyCode == 0 ? evt.charCode : evt.keyCode;
            var value = Number(evt.target.value + evt.key) || 0;

            if ((keyCode >= 37 && keyCode <= 40) || (keyCode == 8 || keyCode == 9 || keyCode == 13) || (keyCode >= 48 && keyCode <= 57)) {
                return (1 <= value);
            }
            return false;
        }
    </script>
@endsection