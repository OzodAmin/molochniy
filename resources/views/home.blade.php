@extends('layouts.app')

@section('title')
    Главная
@endsection


@section('content')

    <section class="cart bgwhite p-b-100">
        <div class="container">

            <div class="row">
                <div class="col-md-4">
                    <b>Товар</b>
                </div>
                <div class="col-md-2">
                    <b>Цена</b>
                </div>
                <div class="col-md-2">
                    <b>Кол-во</b>
                </div>
                <div class="col-md-2">
                    <b> </b>
                </div>
                <div class="col-md-2">
                    <b>Сумма</b>
                </div>
            </div>

            <hr>
            {!! Form::open(['route' => 'order']) !!}

            @foreach($products as $product)
                <?php
                if (old('quantity'))
                    $oldQuantityInput = old('quantity');

                if (isset($oldQuantityInput) && in_array($product->id, session()->get('errorProductIds'))) {
                    $errorClass = 'bo4';
                    $errorTitle = 'bg-danger';
                } else {
                    $errorClass = 'bo5';
                    $errorTitle = 'bg-info';
                }
                ?>
                <div class="row p-t-10">
                    <div class="col-md-4 p-t-5 {{ $errorTitle }}" role="alert">
                        <h4 class="p-3"><b>{{ $product->title }}</b></h4>
                    </div>
                    <div class="col-md-2 p-t-5">
                        <b id="price_{{ $product->id }}">{{ $product->price }}</b>
                        <span class="font-weight-bold">сум</span>
                    </div>
                    @if($product->quantity > 0)

                        <div class="col-md-2 p-t-5">
                            {{ $product->quantity }} шт
                        </div>

                        <div class="col-md-2 p-t-5">
                            <div class="flex-w {{ $errorClass }} of-hidden w-size17">

                                <input
                                        class=" m-text18 t-center num-product"
                                        min="0"
                                        id="productQuantity_{{ $product->id }}"
                                        type="number"
                                        name="quantity[{{$product->id}}]"
                                        onchange="qtyChange({{ $product->id. ','.$product->quantity }}, this.value)"
                                        onkeypress="javascript:return isNumber(event)"
                                        value="{{ isset($oldQuantityInput) ? $oldQuantityInput[$product->id] : '' }}">
                            </div>
                        </div>

                        <div class="col-md-2 p-t-5">
                            <p id="total_{{$product->id}}" class="prices">0</p>
                        </div>
                    @else
                        <div class="col-md-6 p-t-5">
                            <b>Нет в наличии </b>
                        </div>
                    @endif
                </div>
            @endforeach

        <!-- Total -->
            <div class="bo9 w-size18 p-l-40 p-r-40 p-t-30 p-b-38 m-t-30 m-r-0 m-l-auto p-lr-15-sm">
                <!--  -->
                <div class="flex-w flex-sb-m p-b-30">
					<span class="m-text22 w-size6 w-full-sm">
						Итого:
					</span>

                    <span class="m-text21 w-size6 w-full-sm" id="totalPrice">
                        </span>
                    <span class="w-size6"> сум</span>
                </div>

                <div class="size15 trans-0-4">
                    <!-- Button -->
                    <button class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4">
                        Оформить заказ
                    </button>
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </section>

@endsection

@section('script')

    <script type="text/javascript">
        function isNumber(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        }

        function qtyChange(id, quantity, val) {
            if (val < quantity || val == quantity) {
                var price = $('#price_' + id).text();
                var numb = price.match(/\d/g);
                numb = numb.join("");
                var result = val * numb;
            } else {
                var result = 0;
                $('#productQuantity_' + id).val(result);
            }
            $('#total_' + id).text(result);
            calculate();
        }

        function calculate() {
            var divs = document.getElementsByClassName("prices");
            var totalCartPrice = 0;

            for (var i = 0; i < divs.length; i += 1)
                totalCartPrice += Number(divs[i].innerHTML);

            $('#totalPrice').html(totalCartPrice);
        }
    </script>
@endsection
