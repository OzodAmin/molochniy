<?php ?>
@extends('admin.layouts.app')

@section('title')
    Категории
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Категории
            <a href="{{ route('categories.create') }}" class="btn btn-success">
                <i class="fa fa-btn fa-plus"></i>
                &nbsp;New Category
            </a>
        </h1>
    </section>

    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table table-striped table-bordered table-condensed">
                <thead>
                <tr>
                    <th>Наименование </th>
                    <th>Действия </th>
                </tr>
                </thead>
                <tbody>

                @foreach ($categories as $key => $category)

                    <tr class="categories-users">
                        <td>{{ $category->name }}</td>
                        <td>
                            <a class="btn btn-primary" href="{{ route('categories.edit',$category->id) }}">
                                <i class="fa fa-btn fa-edit"></i> Edit
                            </a>
                            <form action="{{ url('backend/categories/'.$category->id) }}" method="POST" style="display: inline-block">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <button type="submit" id="delete-task-{{ $category->id }}" class="btn btn-danger">
                                    <i class="fa fa-btn fa-trash"></i> Delete
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{ $categories->links() }}
@endsection