@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
        @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

<div class="box-body">
    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Название:</label>
        {{ Form::text('name', isset($category) ? $category->name : null, ['class' => 'form-control', 'required' => true]) }}
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Slug</label>
        {{ Form::text( 'slug',
                    isset($category) ? $category->slug : null,
                    ['class' => 'form-control',
                    'disabled' => 'true'])
                }}
    </div>
</div>

<div class="box-footer">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('categories.index') !!}" class="btn btn-default">Отменить</a>
</div>

