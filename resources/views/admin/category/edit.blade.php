@extends('admin.layouts.app')
@section('title')
    Редактировать категорию
@endsection
@section('content')
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Редактировать категорию</h3>
                    </div>

                    {!! Form::model($category, ['route' => ['categories.update', $category->id], 'method' => 'patch']) !!}

                    @include('admin.category.fields')

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </section>
@endsection