<?php ?>
@extends('admin.layouts.app')
@section('title')
    Создать категорию
@endsection
@section('content')
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Создать категорию</h3>
                    </div>

                    {!! Form::open(['route' => 'categories.store']) !!}

                    @include('admin.category.fields')

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </section>
@endsection