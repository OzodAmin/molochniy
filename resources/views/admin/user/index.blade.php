<?php ?>
@extends('admin.layouts.app')

@section('title')
    Пользователи
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('bootstrap-select/css/bootstrap-select.css') }}">
    <script src="{{ asset('bootstrap-select/js/bootstrap-select.js') }}"></script>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Пользователи
            <a href="{{ route('users.create') }}" class="btn btn-success">
                <i class="fa fa-btn fa-plus"></i>
                &nbsp;Новый пользователь
            </a>
        </h1>
    </section>
    <div class="panel panel-default">
        <div class="panel-body">
            {!! Form::open(['method'=>'GET','url'=>Request::fullUrl(),'role'=>'search', 'autocomplete' => 'off'])  !!}
                <div class="form-group col-md-12">
                    <select class="selectpicker form-control"
                            name="userId"
                            data-container="body"
                            data-live-search="true"
                            title="Поиск пользователя"
                            data-hide-disabled="true">
                        @foreach($usersArray as $key=>$index)
                            <option value="{{ $key }}">{{ $index }}</option>
                        @endforeach
                    </select>
                </div>

                {!! Form::submit('Фильтровать', ['class' => 'btn btn-primary  btn-block col-md-4']) !!}
            {!! Form::close() !!}


            <table class="table table-striped table-bordered table-condensed">
                <thead>
                <tr>
                    <th>Наименование</th>
                    <th>Логин</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($users as $user)

                    <tr class="categories-users">
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->username }}</td>
                        <td>
                            {{--<a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">--}}
                            {{--<i class="fa fa-btn fa-edit"></i> Edit--}}
                            {{--</a>--}}
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#userEditModal_{{ $user->id }}">
                                <i class="fa fa-btn fa-edit"></i> Edit
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="userEditModal_{{ $user->id }}" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">{{ $user->name }}</h4>
                                        </div>
                                        {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'patch']) !!}
                                        <div class="modal-body">


                                            <div class="form-group @error('name') has-error @enderror">
                                                <label for="name" class="col-md-4 col-form-label text-md-right">Наименование</label>

                                                <input id="name" type="text"
                                                       class="form-control"
                                                       name="name"
                                                       value="{{ $user->name }}"
                                                       autocomplete="name">
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group @error('username') has-error @enderror">
                                                <label for="username" class="col-md-4 col-form-label text-md-right">Логин</label>

                                                <input id="username" type="text"
                                                       class="form-control"
                                                       name="username"
                                                       value="{{ $user->username }}"
                                                       required autocomplete="username">
                                                @error('username')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>


                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                            </button>
                                            {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>

                            <form action="{{ url('backend/users/'.$user->id) }}" method="POST"
                                  style="display: inline-block">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <button type="submit" id="delete-task-{{ $user->id }}" class="btn btn-danger">
                                    <i class="fa fa-btn fa-trash"></i> Delete
                                </button>
                            </form>

                            <button type="button" class="btn btn-warning" data-toggle="modal"
                                    data-target="#userPasswordChange_{{ $user->id }}">
                                <i class="fa fa-btn fa-lock"></i> Change password
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="userPasswordChange_{{ $user->id }}" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">{{ $user->name }}</h4>
                                        </div>
                                        {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'patch']) !!}
                                        <div class="modal-body">
                                            <div class="form-group @error('password') has-error @enderror">
                                                <label for="password" class="col-md-4 col-form-label text-md-right">Пароль</label>

                                                <input id="password" type="password"
                                                       class="form-control @error('password') is-invalid @enderror"
                                                       name="password" required autocomplete="new-password">

                                                @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label for="password-confirm"
                                                       class="col-md-4 col-form-label text-md-right">Повторить
                                                    пароль</label>

                                                <input id="password-confirm" type="password" class="form-control"
                                                       name="password_confirmation" required
                                                       autocomplete="new-password">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                            </button>
                                            {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>

                            <a href="{{ route('accounting.show', ['id' => $user->id]) }}" type="button"
                               class="btn btn-info">
                                <i class="fa fa-btn fa-calculator"></i> Бухгалтерия
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{ $users->links() }}
@endsection

@section('script')
    <script src="{{ asset('bootstrap-select/js/bootstrap-select.js') }}"></script>
@endsection