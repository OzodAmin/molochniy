@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
        @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

<div class="box-body">
    <div class="form-group @error('name') has-error @enderror">
        <label for="name" class="col-md-4 col-form-label text-md-right">Наименование</label>

        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name"
               value="{{ isset($user) ? $user->name : old('name') }}" required autocomplete="name" autofocus>

        @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class="form-group @error('username') has-error @enderror">
        <label for="username" class="col-md-4 col-form-label text-md-right">Логин</label>

        <input id="username" type="text" class="form-control @error('username') is-invalid @enderror"
               name="username"
               value="{{ isset($user) ? $user->username : old('username') }}" required autocomplete="username">

        @error('username')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class="form-group @error('password') has-error @enderror">
        <label for="password" class="col-md-4 col-form-label text-md-right">Пароль</label>

        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
               name="password" required autocomplete="new-password">

        @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Повторить пароль</label>

        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required
                   autocomplete="new-password">
    </div>

</div>

<div class="box-footer">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('products.index') !!}" class="btn btn-default">Отменить</a>
</div>

