@extends('admin.layouts.app')
@section('title')
    Редактировать пользователь
@endsection
@section('content')
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Редактировать пользователь</h3>
                    </div>

                    {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'patch']) !!}

                    @include('admin.user.fields')

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </section>
@endsection