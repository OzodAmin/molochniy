<?php ?>
@extends('admin.layouts.app')
@section('title')
    Создать пользователь
@endsection
@section('content')
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Создать пользователь</h3>
                    </div>

                    {!! Form::open(['route' => 'users.store']) !!}

                    @include('admin.user.fields')

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </section>
@endsection