<?php ?>
@extends('admin.layouts.app')

@section('title')
    {{ $user->name }}
@endsection

@section('css')
    <link href="{{ asset('DataTables/DataTables-1.10.18/css/jquery.dataTables.min.css') }}" rel="stylesheet">
@endsection

@section('content')

    <div class="panel panel-default">
        <div class="panel-body">
            <h3>{{ $user->name }}</h3>

            {{--<button class="btn btn-primary  btn-block" type="button" data-toggle="collapse"--}}
            {{--data-target="#collapseExample"--}}
            {{--aria-expanded="false" aria-controls="collapseExample">--}}
            {{--Добавить за {{ \Carbon\Carbon::now()->format('d/m/Y') }}--}}
            {{--</button>--}}

            {{--<div class="collapse" id="collapseExample">--}}
            {{--<div class="well">--}}
            {{--{!! Form::open(['route' => 'accounting.store']) !!}--}}
            {{--<div class="form-group col-md-4">--}}
            {{--<label for="field_1">Внутренний:</label>--}}
            {{--{{ Form::text('field_1', null, ['min' => '0', 'class' => 'form-control', 'required' => true]) }}--}}
            {{--</div>--}}

            {{--<div class="form-group col-md-4">--}}
            {{--<label for="field_2">Заказы:</label>--}}
            {{--{{ Form::text('field_2', null, ['min' => '0', 'class' => 'form-control', 'required' => true]) }}--}}
            {{--</div>--}}

            {{--<div class="form-group col-md-4">--}}
            {{--<label for="field_3">Инкассация:</label>--}}
            {{--{{ Form::text('field_3', null, ['min' => '0', 'class' => 'form-control', 'required' => true]) }}--}}
            {{--</div>--}}

            {{--<div class="clearfix"></div>--}}

            {{--<div class="form-group col-md-4">--}}
            {{--<label for="field_4">Терминал:</label>--}}
            {{--{{ Form::text('field_4', null, ['min' => '0', 'class' => 'form-control', 'required' => true]) }}--}}
            {{--</div>--}}

            {{--<div class="form-group col-md-4">--}}
            {{--<label for="field_5">Деньги:</label>--}}
            {{--{{ Form::text('field_5', null, ['min' => '0', 'class' => 'form-control', 'required' => true]) }}--}}
            {{--</div>--}}

            {{--<input type="hidden" value="{{ $user->id }}" name="user_id">--}}

            {{--{!! Form::submit('Сохранить', ['class' => 'btn btn-primary  btn-block col-md-4']) !!}--}}
            {{--<div class="clearfix"></div>--}}
            {{--{!! Form::close() !!}--}}
            {{--</div>--}}
            {{--</div>--}}

            <table class="table table-striped table-bordered" id="table">
                <thead>
                <tr>
                    <th>Число</th>
                    <th>Заказы</th>
                    <th>Внутренний</th>
                    <th>Инкассация</th>
                    <th>Терминал</th>
                    <th>Остаток от заказа</th>
                    <th>Остаток в общем</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('DataTables/Buttons-1.5.6/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('DataTables/Buttons-1.5.6/js/buttons.flash.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="{{ asset('DataTables/pdfmake-0.1.36/pdfmake.min.js') }}"></script>
    <script src="{{ asset('DataTables/pdfmake-0.1.36/vfs_fonts.js') }}"></script>
    <script src="{{ asset('DataTables/Buttons-1.5.6/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('DataTables/Buttons-1.5.6/js/buttons.print.min.js') }}"></script>

    <script>
        $(function () {
            $('#table').DataTable({
                processing: true,
                serverSide: true,
                ordering: false,
                info: false,
                searching: false,
                paging: true,
                ajax: {
                    url: '{!! route( 'accounting.create', ['id' => $user->id] ) !!}',
                    method: "get",
                },
                columns: [
                    {data: 'created_at'},
                    {data: 'orderSumForToday'},
                    {data: 'innerSum'},
                    {data: 'taxSum'},
                    {data: 'terminalSum'},
                    {data: 'balanceSum'},
                    {data: 'balance_at_all'}
                ],
                columnDefs: [{
                    targets: 0, render: function (data) {
                        return moment(data).format('DD/MM/YYYY');
                    }
                }],
                dom: 'Bfrtip',
                buttons: [
                    'print', 'excel'
                ]
            });
        });
    </script>
@endsection