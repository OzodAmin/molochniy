<?php ?>
@extends('admin.layouts.app')

@section('title')
    Бухгалтерия
@endsection

@section('css')
    <link href="{{ asset('DataTables/DataTables-1.10.18/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bootstrap-select/css/bootstrap-select.min.css') }}">
@endsection

@section('content')

    <div class="panel panel-default">
        <div class="panel-body">

            @csrf
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control" name="dateFilter" id="datepicker"
                           value="{{ old('dateFilter') }}">
                </div>
            </div>

            <table class="table table-striped table-bordered" id="table">
                <thead>
                <tr>
                    <th>Дата</th>
                    <th>Магазин</th>
                    <th>Заказы</th>
                    <th>Внутренний</th>
                    <th>Инкассация</th>
                    <th>Терминал</th>
                    <th>Остаток от заказа</th>
                    <th>Остаток в общем</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                <tr>
                    <th colspan="2" style="text-align:right">Итого:</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('DataTables/Buttons-1.5.6/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('DataTables/Buttons-1.5.6/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('DataTables/jszip.min.js') }}"></script>
    <script src="{{ asset('DataTables/moment.min.js') }}"></script>
    <script src="{{ asset('DataTables/pdfmake-0.1.36/pdfmake.min.js') }}"></script>
    <script src="{{ asset('DataTables/pdfmake-0.1.36/vfs_fonts.js') }}"></script>
    <script src="{{ asset('DataTables/Buttons-1.5.6/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('DataTables/Buttons-1.5.6/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('bootstrap-select/js/bootstrap-select.min.js') }}"></script>

    <script>
        $(function () {

            var _token = $("input[name=_token]").val();
            //Date picker
            $('#datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            });

            $('#datepicker').change(function () {
                var orderDate = this.value;
                $("#table").dataTable().fnDestroy();
                $('#table').DataTable({
                    processing: true,
                    serverSide: true,
                    ordering: false,
                    info: false,
                    searching: false,
                    paging: false,
                    ajax: {
                        url: '{!! route( 'accountingByDay' ) !!}',
                        method: "post",
                        data: {orderDate: orderDate, _token: _token},
                    },
                    columns: [
                        {data: 'created_at'},
                        {data: 'name'},
                        {data: 'orderSumForToday'},
                        {data: 'innerSum'},
                        {data: 'taxSum'},
                        {data: 'terminalSum'},
                        {data: 'balanceSum'},
                        {data: 'balance_at_all'}
                    ],
                    columnDefs: [{
                        targets: 0, render: function (data) {
                            return moment(data).format('DD/MM/YYYY');
                        }
                    }],
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'print',
                            footer: true
                        },
                        {
                            extend: 'excel',
                            footer: true
                        },
                    ],
                    createdRow: function (row, data, index) {
                        if (data['balance_at_all'] * 1 >= 10000000)
                            $(row).addClass('danger');
                        else if (data['balance_at_all'] * 1 >= 6000000)
                            $(row).addClass('warning');
                        else if (data['balance_at_all'] * 1 >= 3000000)
                            $(row).addClass('success');
                    },
                    "footerCallback": function (row, data, start, end, display) {
                        var api = this.api(), data;

                        // Remove the formatting to get integer data for summation
                        var intVal = function (i) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '') * 1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };

                        // Total over this page
                        pageOrdersTotal = api
                            .column(2, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        pageInnerTotal = api
                            .column(3, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        pageTaxTotal = api
                            .column(4, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        pageTerminalTotal = api
                            .column(5, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        pageCurrentTotal = api
                            .column(6, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        pageTotal = api
                            .column(7, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        // Update footer
                        $(api.column(2).footer()).html(pageOrdersTotal);
                        $(api.column(3).footer()).html(pageInnerTotal);
                        $(api.column(4).footer()).html(pageTaxTotal);
                        $(api.column(5).footer()).html(pageTerminalTotal);
                        $(api.column(6).footer()).html(pageCurrentTotal);
                        $(api.column(7).footer()).html(pageTotal);
                    }
                });
            });
        });
    </script>
@endsection