<?php ?>
@extends('admin.layouts.app')

@section('title')
    Бухгалтерия
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('bootstrap-select/css/bootstrap-select.css') }}">
    <script src="{{ asset('bootstrap-select/js/bootstrap-select.js') }}"></script>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Бухгалтерия
        </h1>
    </section>
    <div class="panel panel-default">
        <div class="panel-body">
            {!! Form::open(['method'=>'GET','url'=>Request::fullUrl(),'role'=>'search', 'autocomplete' => 'off'])  !!}
            <div class="form-group col-md-12">
                <select class="selectpicker form-control"
                        name="userId"
                        data-container="body"
                        data-live-search="true"
                        title="Поиск пользователя"
                        data-hide-disabled="true">
                    @foreach($usersArray as $key=>$index)
                        <option value="{{ $key }}">{{ $index }}</option>
                    @endforeach
                </select>
            </div>

            {!! Form::submit('Фильтровать', ['class' => 'btn btn-primary  btn-block col-md-4']) !!}
            {!! Form::close() !!}


            <table class="table table-striped table-bordered table-condensed">
                <thead>
                <tr>
                    <th>Магазин</th>
                    <th>Остаток</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($users as $user)
<?php
//dd($user->accounting)
?>
                    <tr class="categories-users">
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->username }}</td>
                        <td>
                            <form action="{{ url('backend/accounting/'.$user->id) }}" method="POST"
                                  style="display: inline-block">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <button type="submit" id="delete-task-{{ $user->id }}" class="btn btn-danger">
                                    <i class="fa fa-btn fa-trash"></i> Снять деньгиphp artisan migrate
                                </button>
                            </form>

                            <a href="{{ route('accounting.show', ['id' => $user->id]) }}" type="button"
                               class="btn btn-info">
                                <i class="fa fa-btn fa-calculator"></i> Бухгалтерия
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{ $users->links() }}
@endsection

@section('script')
    <script src="{{ asset('bootstrap-select/js/bootstrap-select.js') }}"></script>
@endsection