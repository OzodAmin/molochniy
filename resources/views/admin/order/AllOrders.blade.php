<?php ?>
@extends('admin.layouts.app')

@section('title')
    Заказы за {{ \Carbon\Carbon::today()->format('d/m/Y') }}
@endsection

@section('css')
    <link href="{{ asset('DataTables/DataTables-1.10.18/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bootstrap-select/css/bootstrap-select.min.css') }}">
@endsection

@section('content')

    <div class="panel panel-default">
        <div class="panel-body">

            @csrf
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control" name="dateFilter" id="datepicker">
                </div>
            </div>

            <table class="table table-striped table-bordered table-condensed" id="table">
                <thead>
                <tr>
                    <th>Дата</th>
                    <th>Магазин</th>
                    <th>Товар</th>
                    <th>Цена</th>
                    <th>Количество</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('DataTables/Buttons-1.5.6/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('DataTables/Buttons-1.5.6/js/buttons.flash.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="{{ asset('DataTables/pdfmake-0.1.36/pdfmake.min.js') }}"></script>
    <script src="{{ asset('DataTables/pdfmake-0.1.36/vfs_fonts.js') }}"></script>
    <script src="{{ asset('DataTables/Buttons-1.5.6/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('DataTables/Buttons-1.5.6/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('bootstrap-select/js/bootstrap-select.min.js') }}"></script>
    <script>

        $(function () {

            var _token = $("input[name=_token]").val();
            //Date picker
            $('#datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            });

            $('#datepicker').change(function () {
                var orderDate = this.value;
                $("#table").dataTable().fnDestroy()
                $('#table').DataTable({
                    processing: true,
                    serverSide: true,
                    ordering: false,
                    info: false,
                    searching: false,
                    paging: false,
                    ajax: {
                        url: '{!! route( 'ordersAllList' ) !!}',
                        method: "post",
                        data: {orderDate: orderDate, _token: _token},
                    },
                    columns: [
                        {
                            data: 'created_at',
                            type: "date ",
                            render: function (data) {
                                var date = new Date(data);
                                var month = date.getMonth() + 1;
                                return date.getDate() + "." + (month.length > 1 ? month : "0" + month) + "." + date.getFullYear();
                            }
                        },
                        {data: 'name', name: 'product.title'},
                        {data: 'title', name: 'product.title'},
                        {data: 'price', name: 'order_product.price'},
                        {data: 'quantity', name: 'order_product.quantity'}
                    ],
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'print',
                            title: orderDate
                        },
                        {
                            extend: 'excel',
                            title: orderDate
                        },
                    ]
                });
            });

        });
    </script>
@endsection