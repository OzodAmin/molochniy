<?php ?>
@extends('admin.layouts.app')

@section('title')
    Аналитика
@endsection

@section('css')
    <link href="{{ asset('DataTables/DataTables-1.10.18/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bootstrap-select/css/bootstrap-select.min.css') }}">
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">

            <div class="row">
                <div class="col-md-offset-3 col-md-6">
                    <button class="btn btn-info btn-lg btn-block" type="button" data-toggle="collapse"
                            data-target="#filter"
                            aria-expanded="false" aria-controls="filter">
                        Фильтр
                    </button>
                </div>
                <div class="col-md-12">
                    <div class="collapse" id="filter">
                        <div class="well">
                            {!! Form::open(['route' => 'ordersReport']) !!}
                            <div class="input-daterange">
                                <div class="col-md-6">
                                    <input type="text" name="start_date" class="form-control"
                                           value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}"/>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="end_date" class="form-control"
                                           value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}"/>
                                </div>
                            </div>

                            <div class="col-md-12" style="padding-bottom: 20px; padding-top: 20px">
                                <select multiple class="selectpicker form-control"
                                        name="products[]"
                                        data-container="body"
                                        data-live-search="true"
                                        data-actions-box="true"
                                        title="Select products"
                                        data-hide-disabled="true"
                                        data-virtual-scroll="false">
                                    @foreach($productsArray as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-12" style="padding-bottom: 20px">
                                <select multiple class="selectpicker form-control"
                                        name="users[]"
                                        data-actions-box="true"
                                        data-container="body"
                                        data-live-search="true"
                                        title="Select users"
                                        data-hide-disabled="true"
                                        data-virtual-scroll="false">
                                    @foreach($uersArray as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>

                            {!! Form::submit('Фильтровать', ['class' => 'btn btn-primary btn-lg btn-block']) !!}

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            @if(isset($orders))
                <table class="table table-striped table-bordered table-condensed" id="table">
                    <thead>
                    <tr>
                        <th>Пользователь</th>
                        <th>Товар</th>
                        <th>Количество</th>
                        <th>Дата</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $order)
                        @foreach ($order->productsPivotTable as $product)
                            <tr>
                                <td>{{ $order->user->name }}</td>
                                <td>{{ $product->title }}</td>
                                <td>{{ $product->pivot->quantity }}</td>
                                <td>{{ $order->created_at }}</td>
                            </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('DataTables/Buttons-1.5.6/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('DataTables/Buttons-1.5.6/js/buttons.flash.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="{{ asset('DataTables/pdfmake-0.1.36/pdfmake.min.js') }}"></script>
    <script src="{{ asset('DataTables/pdfmake-0.1.36/vfs_fonts.js') }}"></script>
    <script src="{{ asset('DataTables/Buttons-1.5.6/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('DataTables/Buttons-1.5.6/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('bootstrap-select/js/bootstrap-select.min.js') }}"></script>
    <script>

        $(function () {

            $('#table').DataTable({
                "paging": false,
                "info": false,
                searching: false,
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel', 'pdf', 'print'
                ]
            });

            $('.input-daterange').datepicker({
                todayBtn: 'linked',
                format: "yyyy-mm-dd",
                autoclose: true
            });

            {{--var oTable = $('#table').DataTable({--}}
            {{--processing: true,--}}
            {{--serverSide: true,--}}
            {{--ordering: false,--}}
            {{--searching: false,--}}
            {{--paging: false,--}}
            {{--ajax: {--}}
            {{--url: '{!! route( 'ordersList' ) !!}',--}}
            {{--data: function (d) {--}}
            {{--d.start_date = $('input[name=start_date]').val();--}}
            {{--d._token = $('input[name=_token]').val();--}}
            {{--d.end_date = $('input[name=end_date]').val();--}}
            {{--d.products = selectednumbers.toString();--}}
            {{--},--}}
            {{--type:"POST",--}}
            {{--traditional: true,--}}
            {{--},--}}
            {{--columns: [--}}
            {{--{data: 'name', name: 'users.name'},--}}
            {{--{data: 'title', name: 'product.title'},--}}
            {{--{data: 'quantity', name: 'order_product.quantity'},--}}
            {{--{data: 'created_at', name: 'orders.created_at', type: 'datetime'}--}}
            {{--],--}}
            {{--dom: 'Bfrtip',--}}
            {{--buttons: [--}}
            {{--'csv', 'excel', 'pdf', 'print'--}}
            {{--]--}}
            {{--});--}}

        });
    </script>
@endsection