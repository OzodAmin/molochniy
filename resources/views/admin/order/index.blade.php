<?php ?>
@extends('admin.layouts.app')

@section('title')
    Заказы
@endsection

@section('css')
    <link href="{{ asset('DataTables/DataTables-1.10.18/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bootstrap-select/css/bootstrap-select.min.css') }}">
@endsection

@section('content')

    <div class="panel panel-default">
        <div class="panel-body">
            <?php
            if (isset($request->dateFilter))
                $date =  $request->dateFilter;
            else
                $date =  \Carbon\Carbon::now()->format('Y-m-d');
            ?>

            {!! Form::open(['method'=>'GET', 'url'=>Request::fullUrl(), 'class' => 'form-inline']) !!}
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control" name="dateFilter" id="datepicker"
                           value="{{ $date }}">
                </div>
            </div>

            {!! Form::submit('Фильтровать', ['class' => 'btn btn-primary btn-lg']) !!}
            {!! Form::close()  !!}

            <h3>Заказы за {{ $date }}</h3>

            <table class="table table-striped table-bordered table-condensed">
                <thead>
                <tr>
                    <th>Наименование</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($orders as $order)

                    <tr class="categories-users">
                        <td>
                            <button id="<?= $order->id; ?>" class="btn btn-success btn-block view_data">
                            <span id="title_<?= $order->id; ?>">
                                Заказы магазина "<?= $order->user->name; ?>" за
                                {{ \Carbon\Carbon::parse($order->created_at)->format('d/m/Y') }}
                            </span>
                                (Итого: <span
                                        id="total_{{ $order->id }}">{{  number_format($order->total_price, 0, '', ' ') }}</span>
                                Сум)
                            </button>
                        </td>
                        <td>
                            {!! Form::open(['method'=>'GET', 'url'=>Request::fullUrl()]) !!}

                                <input type="hidden" name="orderId" value="{{ $order->id }}">
                                <input type="hidden" name="dateFilter"
                                       value="{{ isset($request) ? $request->dateFilter : \Carbon\Carbon::now()->format('Y-m-d') }}">

                                <button type="submit" id="delete-task-{{ $order->id }}" class="btn btn-danger">
                                    <i class="fa fa-btn fa-trash"></i> Delete
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div id="dataModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id="modalTitle"></h4>
                        </div>
                        <div class="modal-body">
                            @csrf
                            <table class="table table-striped table-bordered table-condensed" id="table">
                                <thead>
                                <tr>
                                    <th>Товар</th>
                                    <th>Цена</th>
                                    <th>Количество</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th colspan="2" style="text-align:right">Итого:</th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('DataTables/Buttons-1.5.6/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('DataTables/Buttons-1.5.6/js/buttons.flash.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="{{ asset('DataTables/pdfmake-0.1.36/pdfmake.min.js') }}"></script>
    <script src="{{ asset('DataTables/pdfmake-0.1.36/vfs_fonts.js') }}"></script>
    <script src="{{ asset('DataTables/Buttons-1.5.6/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('DataTables/Buttons-1.5.6/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('bootstrap-select/js/bootstrap-select.min.js') }}"></script>
    <script>

        $(function () {
            //Date picker
            $('#datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
            })

            var order_id = 0;
            var _token = $("input[name=_token]").val();

            $('.view_data').click(function () {

                order_id = $(this).attr("id");
                var ordername = $('#title_' + order_id).html();
                var orderTotal = $('#total_' + order_id).html();

                $('#modalTitle').html(ordername);
                $('#dataModal').modal("show");
                $("#table").dataTable().fnDestroy()

                $('#table').DataTable({
                    processing: true,
                    serverSide: true,
                    ordering: false,
                    info: false,
                    searching: false,
                    paging: false,
                    ajax: {
                        url: '{!! route( 'ordersList' ) !!}',
                        method: "post",
                        data: {order_id: order_id, _token: _token},
                    },
                    columns: [
                        {data: 'title', name: 'product.title'},
                        {data: 'price', name: 'order_product.price'},
                        {data: 'quantity', name: 'order_product.quantity'}
                    ],
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'print',
                            title: ordername,
                            footer: true
                        },
                        {
                            extend: 'excel',
                            title: ordername,
                            footer: true
                        },
                    ],
                    footerCallback: function (row, data, start, end, display) {
                        var api = this.api(), data;
                        // Update footer
                        $(api.column(2).footer()).html(
                            orderTotal + ' Сум'
                        );
                    }
                });

            });

        });
    </script>
@endsection