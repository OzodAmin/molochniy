@extends('admin.layouts.app')
@section('title')
    Редактировать сессию
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('timepicker/bootstrap-timepicker.min.css') }}">
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Редактировать сессию</h3>
                    </div>

                    {!! Form::model($system, ['route' => ['system.update', $system->id], 'method' => 'patch']) !!}

                    @if (count($errors) > 0)
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            @foreach ($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif

                    <div class="box-body">
                        <div class="form-group {{ $errors->has('systemBegin') ? ' has-error' : '' }}">
                            <label for="systemBegin">Начало сессии:</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-clock-o"></i>
                                </div>

                                {{ Form::text('systemBegin', $system->systemBegin, ['class' => 'form-control timepicker', 'required' => true]) }}
                            </div>

                        </div>
                        <div class="form-group {{ $errors->has('systemEnd') ? ' has-error' : '' }}">
                            <label for="exampleInputEmail1">Конец сессии:</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-clock-o"></i>
                                </div>

                                {{ Form::text('systemEnd', $system->systemEnd, ['class' => 'form-control timepicker', 'required' => true]) }}
                            </div>
                        </div>
                    </div>

                    <div class="box-footer">
                        {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
                        <a href="{!! route('system') !!}" class="btn btn-default">Отменить</a>
                    </div>


                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script src="{{ asset('timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script>
        $(function () {
            //Timepicker
            $('.timepicker').timepicker({
                showInputs: false,
                showMeridian: false,
            })
        })
    </script>
@endsection