<?php ?>
@extends('admin.layouts.app')

@section('title')
    Продукты
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Продукты
            <a href="{{ route('products.create') }}" class="btn btn-success">
                <i class="fa fa-btn fa-plus"></i>
                &nbsp;Новый продукт
            </a>
        </h1>
    </section>

    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table table-striped table-bordered table-condensed">
                <thead>
                <tr>
                    <th>Порядок</th>
                    <th>Наименование</th>
                    <th>Цена</th>
                    <th>Кол-во</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($products as $key => $product)

                    <tr>
                        <td>
                            <button id="{{ $product->id }}" class="btn btn-primary btn-block editOrder">
                                {{ $product->order }}
                            </button>
                        </td>
                        <td>
                            <span id="productTitle_{{ $product->id }}">
                                {{ $product->title }}
                            </span>
                        </td>
                        <td>{{ number_format($product->price, 0, '', ' ') }} сум</td>
                        <td contenteditable="true"
                            onblur="updateQuantity({{ $product->id }})"
                            id="updateQuantity_{{ $product->id }}">
                            {{ $product->quantity }}
                        </td>
                        <td>
                            <a class="btn btn-primary" href="{{ route('products.edit',$product->id) }}">
                                <i class="fa fa-btn fa-edit"></i> Edit
                            </a>
                            <form action="{{ url('backend/products/'.$product->id) }}" method="POST"
                                  style="display: inline-block">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <button type="submit" id="delete-task-{{ $product->id }}" class="btn btn-danger">
                                    <i class="fa fa-btn fa-trash"></i> Delete
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div id="dataModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id="modalTitle"></h4>
                        </div>
                        <div class="modal-body">
                            <form action="{{ route('updateOrder') }}" method="POST">
                                <div class="form-group col-md-6">
                                    <label for="exampleInputEmail1">Old order:</label>
                                    <input type="text" class="form-control productInputOldOrder" disabled="true">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputEmail1">New order:</label>
                                    <input type="text" class="form-control" name="newOrder" id="productInputNewOrder" required>
                                </div>
                                <input type="hidden" name="productId" id="productInputId">
                                <input type="hidden" name="oldOrder" id="productInputOldOrder" class="productInputOldOrder">
                                {{ csrf_field() }}
                                {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('sweetalert/sweetalert.min.js') }}"></script>

    <script type="text/javascript">
        $(function () {
            $('.editOrder').click(function () {
                var productId = $(this).attr("id");
                var productName = $('#productTitle_' + productId).html();
                var order = $(this).html();
                $('#modalTitle').html(productName);
                $('#dataModal').modal("show");
                $('#productInputId').val(productId);
                $('.productInputOldOrder').val(order);
            });
        });

        function updateQuantity(id) {
            var quantity = $("#updateQuantity_" + id).html();
            var token = document.getElementsByName("_token")[0].value;
            $.ajax({
                type: "POST",
                url: "{{ url('backend/updateQuantity') }}",
                data: {
                    quantity: quantity,
                    id: id,
                    _token: token
                },
                success: function (response) {
                    swal("Success", response, "success");
                    console.log(response);
                }
            });
        }
    </script>
@endsection