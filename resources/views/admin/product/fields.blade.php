@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
        @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

<div class="box-body">
    <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Название:</label>
        {{ Form::text('title', isset($product) ? $product->title : null, ['class' => 'form-control', 'required' => true]) }}
    </div>
    <div class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Категория:</label>
        {!! Form::select('category_id', ['' => 'Select'] + $categoriesArray, null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group {{ $errors->has('price') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Цена:</label>
        {{ Form::text('price',isset($product) ? $product->price : null,
                                    [
                                        'id' => 'price',
                                        'class' => 'form-control',
                                        'onkeypress' => 'javascript:return isNumber(event)',
                                        'placeholder' => 'Цена',
                                        'required' => true
                                    ])
        }}
    </div>
</div>

<div class="box-footer">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('products.index') !!}" class="btn btn-default">Отменить</a>
</div>

@section('script')
    <script>
        function isNumber(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        }
    </script>
@endsection

