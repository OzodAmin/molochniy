<?php ?>
@extends('admin.layouts.app')
@section('title')
    Создать продукт
@endsection
@section('content')
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Создать продукт</h3>
                    </div>

                    {!! Form::open(['route' => 'products.store']) !!}

                    @include('admin.product.fields')

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </section>
@endsection