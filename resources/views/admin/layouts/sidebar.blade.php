<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header"> </li>
            <li>
                <a href="{{ route('users.index') }}">
                    <i class="fa fa-user"></i>
                    <span>Пользователи</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-map-marker"></i>
                    <span>Локация</span>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('backend/locationAdd') }}"><i class="fa fa-circle-o"></i> Добавить</a></li>
                    <li><a href="{{ url('backend/locationList') }}"><i class="fa fa-circle-o"></i> Все</a></li>
                </ul>
            </li>
            <li>
                <a href="{{ route('categories.index') }}">
                    <i class="fa fa-share"></i>
                    <span>Категории</span>
                </a>
            </li>
            <li>
                <a href="{{ route('products.index') }}">
                    <i class="fa fa-apple"></i>
                    <span>Продукты </span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dollar"></i>
                    <span>Заказы</span>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('ordersView') }}"><i class="fa fa-circle-o"></i> Заказы по пользователям</a></li>
                    <li><a href="{{ route('ordersAllView') }}"><i class="fa fa-circle-o"></i> Заказы за весь день</a></li>
                </ul>
            </li>
            <li>
                <a href="{{ route('ordersReportView') }}">
                    <i class="fa fa-line-chart"></i>
                    <span>Аналитика</span>
                </a>
            </li>
            <li>
                <a href="{{ route('accounting.edit', ['id' => 1]) }}">
                    <i class="fa fa-calculator"></i>
                    <span>Бухгалтерия </span>
                </a>
            </li>
            {{--<li class="treeview">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-calculator"></i>--}}
                    {{--<span>Бухгалтерия</span>--}}
                    {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="{{ route('accounting.index') }}"><i class="fa fa-circle-o"></i> Бухгалтерия по пользователям</a></li>--}}
                    {{--<li><a href="{{ route('accounting.edit', ['id' => 1]) }}"><i class="fa fa-circle-o"></i> Бухгалтерия за весь день</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            <li>
                <a href="{{ route('system') }}">
                    <i class="fa fa-line-chart"></i>
                    <span>Системные параметры</span>
                </a>
            </li>
            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out"></i>
                    <span>Выход</span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
    </section>
</aside>