<!DOCTYPE html>
<html lang="en">
<head>
    <title>Location list</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
</head>
<body>
<style>
    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
    }

    #map {
        height: 100%;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<div id="map"></div>

<!------ Include the above in your HEAD tag ---------->
<script>
    var map;
    var marker;
    var infowindow;
    var red_icon = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png';
    var locations = <?= $locationsEncoded; ?>;

    function initMap() {
        var tashkent = {lat: 41.311081, lng: 69.240562};
        infowindow = new google.maps.InfoWindow();
        map = new google.maps.Map(document.getElementById('map'), {
            center: tashkent,
            zoom: 12.5
        });


        var i;
        for (i = 0; i < locations.length; i++) {

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map,
                icon: red_icon,
                html: document.getElementById('form')
            });

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    $("#id").val(locations[i][0]);
                    $("#manual_name").val(locations[i][3]);
                    $("#manual_description").val(locations[i][4]);
                    $("#form").show();
                    infowindow.setContent(marker.html);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }

        var centerControlDiv = document.createElement('div');
        var centerControl = new CenterControl(centerControlDiv, map);

        centerControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);
    }

    function CenterControl(controlDiv, map) {

        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.style.backgroundColor = '#fff';
        controlUI.style.border = '2px solid #fff';
        controlUI.style.borderRadius = '3px';
        controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
        controlUI.style.cursor = 'pointer';
        controlUI.style.marginBottom = '22px';
        controlUI.style.textAlign = 'center';
        controlUI.title = 'Click to open admin panel';
        controlDiv.appendChild(controlUI);

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.style.color = 'rgb(25,25,25)';
        controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
        controlText.style.fontSize = '16px';
        controlText.style.lineHeight = '38px';
        controlText.style.paddingLeft = '5px';
        controlText.style.paddingRight = '5px';
        controlText.innerHTML = 'Назад';
        controlUI.appendChild(controlText);

        // Setup the click event listeners: simply set the map to Chicago.
        controlUI.addEventListener('click', function() {
            window.location = "{{ url('backend') }}";
        });

    }

    function saveData() {
        var deleted = document.getElementById('delete').checked ? 1 : 0;
        var id = document.getElementById('id').value;
        var name = document.getElementById('manual_name').value;
        var address = document.getElementById('manual_description').value;
        var url = 'locationUpdate?id=' + id + '&name=' + name + '&address=' + address + '&delete=' + deleted;
        downloadUrl(url, function (data, responseCode) {
            if (responseCode === 200 && data.length > 1) {
                infowindow.close();
                window.location.reload(true);
            } else {
                infowindow.setContent("<div style='color: purple; font-size: 25px;'>Inserting Errors</div>");
            }
        });
    }


    function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function () {
            if (request.readyState == 4) {
                callback(request.responseText, request.status);
            }
        };

        request.open('GET', url, true);
        request.send(null);
    }


</script>

<div style="display: none" id="form">
    <table class="map1">
        <tr>
            <td><a>Shop name:</a></td>
            <td><input type='text' id='manual_name' placeholder='Name'></td>
        </tr>
        <tr>
            <input name="id" type='hidden' id='id'/>
            <td><a>Address:</a></td>
            <td><textarea id='manual_description' placeholder='Address'></textarea></td>
        </tr>
        <tr>
            <td><b>Delete Location ?:</b></td>
            <td><input id='delete' type='checkbox' name='delete'></td>
        </tr>

        <tr>
            <td></td>
            <td><input type='button' value='Save' onclick='saveData()'/></td>
        </tr>
    </table>
</div>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?language=en&key=AIzaSyBWzUigK_PRcet60Ot8V1tKgR14QNwtHBY&callback=initMap">
</script>