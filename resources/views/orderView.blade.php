@extends('layouts.app')

@section('title')
    Заказ
@endsection

@section('content')

    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{ session('success') }}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <section class="bg-title-page p-b-50 flex-col-c-m"
             style="background-image: url({{ asset('images/heading-pages-01.jpg') }});">
        <h2 class="l-text2 t-center">
            Заказ магазина "{{ $order->user->name }}"
            за {{ \Carbon\Carbon::parse($order->created_at)->format('d/m/Y') }}
        </h2>
    </section>

    <!-- Cart -->
    <section class="cart bgwhite p-t-10 p-b-100">
        <div class="container">
            <!-- Cart item -->
            <div class="container-table-cart pos-relative">
                <div class="wrap-table-shopping-cart bgwhite">
                    <table class="table-shopping-cart">
                        <tr class="table-head">
                            <th class="col-md-6">Продукт</th>
                            <th class="col-md-2">Цена</th>
                            <th class="col-md-2">Количество</th>
                            <th class="col-md-2">Сумма</th>
                        </tr>
                        @foreach($order->productsPivotTable as $orderS)
                            <tr class="table-row">
                                <td class="col-md-6">{{ $orderS->title }}</td>
                                <td class="col-md-2">{{ number_format($orderS->pivot->price, 0, '', ' ') }} сум</td>
                                <td class="col-md-2">{{ $orderS->pivot->quantity }} шт</td>
                                <td class="col-md-2">{{ number_format($orderS->pivot->price * $orderS->pivot->quantity, 0, '', ' ') }}
                                    сум
                                </td>
                            </tr>
                        @endforeach

                    </table>
                </div>
            </div>

            <!-- Total -->
            <div class="bo9 w-size18 p-l-40 p-r-40 p-t-30 p-b-38 m-t-30 m-r-0 m-l-auto p-lr-15-sm">
                <!--  -->
                <div class="flex-w flex-sb-m p-b-12">
					<span class="s-text18 w-size19 w-full-sm">
						Итого:
					</span>

                    <span class="m-text21 w-size20 w-full-sm">
						{{ number_format($order->total_price, 0, '', ' ') }} сум
					</span>
                </div>
            </div>
        </div>
    </section>
@endsection