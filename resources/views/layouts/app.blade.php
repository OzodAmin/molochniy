<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="icon" type="image/png" href="{{ asset('images/favicon.png') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('admin/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/themify/themify-icons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/elegant-font/html-css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/animate/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/animsition/css/animsition.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/css-hamburgers/hamburgers.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/slick/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/util.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">

    @yield('css')
</head>
<body>

<header class="header1">
    <!-- Header desktop -->
    <div class="container-menu-header">

        <div class="wrap_header">
            <!-- Logo -->
            <a href="{{ url('/') }}" class="logo">
                <img src="{{ asset('images/logo.png') }}" alt="IMG-LOGO">
            </a>

            <!-- Menu -->
            {{--<div class="wrap_menu">--}}
            {{--<nav class="menu">--}}
            {{--<ul class="main_menu">--}}
            {{--@foreach($categories as $category)--}}
            {{--<li class="sale-noti">--}}
            {{--<a href="{{ route('category',$category->slug) }}">{{ $category->name }}</a>--}}
            {{--</li>--}}
            {{--@endforeach--}}
            {{--</ul>--}}
            {{--</nav>--}}
            {{--</div>--}}

            @auth
                <div class="header-icons">
                    <p>{{ Auth::user()->name }}</p>

                    @role('admin')
                    <span class="linedivide1"></span>
                    <a href="{{ url('backend') }}" class="header-wrapicon1 dis-block">
                        <img src="{{ asset('images/user.png') }}" class="header-icon1" alt="ICON">
                    </a>
                    @endrole

                    @if(isset($hasAccounting) && $hasAccounting !== true)
                        <span class="linedivide1"></span>
                        <a href="{{ url('accountingIndex') }}" class="header-wrapicon1 dis-block">
                            <img src="{{ asset('images/accounting.png') }}" class="header-icon1" alt="ICON">
                        </a>
                    @endif

                    <span class="linedivide1"></span>

                    <div class="header-wrapicon2">
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <img src="{{ asset('images/exit.png') }}" class="header-icon1" alt="ICON">
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            @endauth
        </div>
    </div>

    <!-- Header Mobile -->
    <div class="wrap_header_mobile">
        <!-- Logo moblie -->
        <a href="/" class="logo-mobile">
            <img src="{{ asset('images/logo.png') }}" alt="IMG-LOGO">
        </a>

        @auth
            <div class="btn-show-menu">
                <!-- Header Icon mobile -->
                <div class="header-icons-mobile">
                    <p>{{ Auth::user()->name }}</p>
                </div>

                <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </div>
            </div>
        @endauth
    </div>

    <!-- Menu Mobile -->
    <div class="wrap-side-menu">
        <nav class="side-menu">
            <ul class="main-menu">
                @role('admin')
                <li class="item-menu-mobile">

                    <a href="{{ url('backend') }}" class="header-wrapicon1 dis-block">
                        <i class="fa fa-user"></i>&nbsp;Админ панель
                    </a>

                </li>
                @endrole
                @if(isset($hasAccounting) && $hasAccounting !== true)
                    <li class="item-menu-mobile">
                        <a href="{{ url('accountingIndex') }}" class="header-wrapicon1 dis-block">
                            <i class="fa fa-calculator"></i>&nbsp;Бухгалтерия
                        </a>
                    </li>
                @endif
                <li class="item-menu-mobile">
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out"></i>&nbsp;Выход
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</header>

@if($errors->any())
    <div class="alert alert-danger fade in">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        @foreach ($errors->all() as $error)
            <p><strong>{{$error}}</strong></p>
        @endforeach
    </div>
@endif

@yield('content')

<!-- Back to top -->
<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
</div>

<script src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/jquery/jquery-3.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/animsition/js/animsition.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/bootstrap/js/popper.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>

<script src="{{ asset('js/main.js') }}"></script>
@yield('script')

</body>
</html>
