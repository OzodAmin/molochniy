<?php

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/', 'HomeController@index')->name('home');
Route::get('category/{slug}', 'HomeController@category')->name('category');
Route::post('order', 'HomeController@order')->name('order');
Route::post('accountingStore', 'HomeController@accountingStore')->name('accountingStore');
Route::view('/accountingIndex', 'accountingIndex');

Route::group(['prefix' => 'backend', 'middleware' => ['role:admin']], function () {

    Route::get('/', 'Admin\BackendController@index');
    Route::resource('accounting', 'Admin\AccountingController');
    Route::post('accountingByDay', 'Admin\AccountingController@accountingByDay')->name('accountingByDay');
    Route::resource('users', 'Admin\UserController');
    Route::resource('categories', 'Admin\CategoryController');
    Route::resource('products', 'Admin\ProductController');
    Route::post('updateOrder', 'Admin\ProductController@updateOrder')->name('updateOrder');
    Route::post('updateQuantity', 'Admin\ProductController@updateQuantity');
    //Orders page
    Route::post('ordersList', 'Admin\OrderController@index')->name('ordersList');
    Route::get('ordersView', 'Admin\OrderController@ordersView')->name('ordersView');
    Route::post('ordersAllList', 'Admin\OrderController@ordersAllList')->name('ordersAllList');
    Route::get('ordersAllView', 'Admin\OrderController@ordersAllView')->name('ordersAllView');

    Route::get('ordersReportView', 'Admin\OrderController@ordersReportView')->name('ordersReportView');
    Route::post('ordersReport', 'Admin\OrderController@ordersReport')->name('ordersReport');

    Route::get('system', 'Admin\SystemController@index')->name('system');
    Route::patch('systemUpdate/{id}', 'Admin\SystemController@update')->name('system.update');

    Route::get('/locationStore','Admin\LocationsController@store');
    Route::get('/locationUpdate','Admin\LocationsController@update');
    Route::get('/locationList','Admin\LocationsController@list');
    Route::view('/locationAdd', 'admin.location.store');
});
