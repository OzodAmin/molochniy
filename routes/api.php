<?php

Route::post('login', 'Api\LoginController@login');
Route::post('refresh', 'Api\LoginController@refresh');

Route::middleware('auth:api')->group(function () {

    Route::post('logout', 'Api\LoginController@logout');
    Route::get('user', 'Api\LoginController@getUser');
    Route::post('accounting', 'Api\AccountingController@store');
    Route::get('isAccounting', 'Api\AccountingController@isAccounting');
    Route::get('isOrder', 'Api\AccountingController@isOrder');

});