<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    public $table = 'order_product';

    public $timestamps = false;

    protected $fillable = ['order_id', 'product_id', 'quantity', 'price'];
}
