<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class Locations extends Model
{
    public $table = 'locations';

    public $timestamps = false;

    protected $fillable = ['lat', 'lng', 'name', 'address'];
}