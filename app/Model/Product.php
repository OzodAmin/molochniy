<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $table = 'product';

    public $timestamps = true;

    protected $fillable = ['title', 'category_id', 'slug', 'order', 'price', 'quantity', ];

    public static $rules = ['title' => 'required|string|min:1|max:255', 'category_id' => 'required', 'price' => 'required'];

    public static function boot() {
        parent::boot();

        static::creating( function($category_translation) {
            $category_translation->slug = str_slug($category_translation->name);

            $latest_slug =static::whereRaw("slug RLIKE '^{$category_translation->slug}(-[0-9]*)?$'")
                ->latest('id')
                ->value('slug');

            if( $latest_slug ) {
                $pieces = explode('-', $latest_slug);
                $number = intval(end($pieces));
                $category_translation->slug .= '-' . ($number + 1);
            }
        });
    }
    public function category()
    {
        return $this->belongsTo('App\Model\Category');
    }

    public function orders()
    {
        return $this->belongsToMany('App\Model\Order');
    }
}
