<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Accounting extends Model
{
    public $table = 'accounting';

    public $timestamps = true;

    protected $fillable = ['user_id', 'orderSumForToday', 'innerSum', 'taxSum', 'terminalSum', 'balanceSum', 'balance_at_all'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
