<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class System extends Model
{
    public $table = 'system';

    public $timestamps = false;

    protected $fillable = ['systemBegin', 'systemEnd'];

    public static $rules = ['systemBegin' => 'required|string|min:1|max:255', 'systemEnd' => 'required|string|min:1|max:255'];
}
