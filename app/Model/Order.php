<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const NEW_ORDER = '1';
    const ACCEPTED_ORDER = '2';
    const DECLINED_ORDER = '3';

    public $table = 'orders';

    public $timestamps = false;

    protected $fillable = ['user_id', 'total_price', 'status', 'note'];

    public function productsPivotTable()
    {
        return $this->belongsToMany('App\Model\Product')->withPivot('quantity', 'price');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
