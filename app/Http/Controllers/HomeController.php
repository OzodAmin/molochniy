<?php

namespace App\Http\Controllers;

use App\Model\Accounting;
use App\Model\Category;
use App\Model\Order;
use App\Model\OrderProduct;
use App\Model\Product;
use App\Model\System;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $today = Carbon::today()->toDateString();
        $order = Order::where('user_id', auth()->id())->whereDate('created_at', '=', $today)->first();
        $hasAccounting = false;
        if (!empty($order)) {
            $accounting = Accounting::where('user_id', auth()->id())->whereDate('created_at', '=', $today)->first();
            if (!empty($accounting))
                $hasAccounting = true;
            return view('orderView', compact('order', 'hasAccounting'));
        }

        $system = System::find(1);
        $beginTime = Carbon::parse($system->systemBegin);
        $endTime = Carbon::parse($system->systemEnd);

        if ($beginTime->gt(Carbon::now())) {
            return view('timer', compact('beginTime'));
        } else if ($endTime->lt(Carbon::now())) {
            $beginTime->addDay(1);
            return view('timer', compact('beginTime'));
        } else {
            //$categories = Category::get();
            $products = Product::where('quantity', '>', 0)->orderBy('order')->get();
            return view('home', compact('products'));
        }
    }

    public function category($slug)
    {
        $categories = Category::get();

        $products = Product::whereHas('category', function ($query) use ($slug) {
            $query->where('slug', 'like', '%' . $slug . '%');
        })->get();

        return view('home', compact('products', 'categories'));
    }

    public function order(Request $request)
    {
        $order = Order::where('user_id', auth()->id())->whereDate('created_at', '=', Carbon::today()->toDateString())->first();
        if (!empty($order)) {
            return view('orderView', compact('order'));
        }

        $totalPrice = 0;
        $errors = array();
        $ordersProducts = array();
        $errorProductIds = array();
        if (isset($request['quantity'])) {
            foreach ($request['quantity'] as $productIdKey => $quantity) {
                if ($request['quantity'][$productIdKey] > 0 && !empty($request['quantity'][$productIdKey])) {
                    $product = Product::find($productIdKey);

                    if ($product->quantity < $quantity) {
                        $errors[] = 'Товар "' . $product->title . '" меньше требуемого.';
                        $errorProductIds[] = $product->id;
                    } else {
                        $orderProduct = array();
                        $orderProduct['product_id'] = $productIdKey;
                        $orderProduct['quantity'] = $quantity;
                        $ordersProducts[] = $orderProduct;
                        $totalPrice += $product->price * $quantity;
                    }

                }
            }
        }

        if (count($errors) > 0 || count($ordersProducts) <= 0) {
            $errors[] = 'Ошибка при оформлении заказа.';
            return Redirect::back()->withErrors($errors)->withInput()->with(compact('errorProductIds', $errorProductIds));
        } else {

            $order = Order::create([
                'user_id' => auth()->id(),
                'total_price' => $totalPrice,
                'status' => Order::NEW_ORDER,
            ]);

            foreach ($ordersProducts as $value) {
                $productId = $value['product_id'];
                $quantity = $value['quantity'];

                $product = Product::find($productId);
                $product->quantity = $product->quantity - $quantity;
                $product->save();

                OrderProduct::create([
                    'order_id' => $order->id,
                    'product_id' => $productId,
                    'quantity' => $quantity,
                    'price' => $product->price,
                ]);

            }
        }
        return redirect()->action('HomeController@index');
//        return view('orderView', compact('order'))->with('success', 'Заказ принят.');
    }

    public function accountingStore(Request $request){
        $userId = auth()->id();

        $lastAccounting = Accounting::where('user_id', '=', $userId)->orderBy('created_at', 'desc')->take(1)->first();
        if (empty($lastAccounting))
            $lastBalance = 0;
        else
            $lastBalance = $lastAccounting->balance_at_all;

        $innerSum = $request->input('innerSum');
        $orderSumForToday = $request->input('orderSumForToday');
        $taxSum = $request->input('taxSum');
        $terminalSum = $request->input('terminalSum');
        $balanceSum = $orderSumForToday - $innerSum - $taxSum - $terminalSum;

        $accounting = new Accounting();
        $accounting->user_id = $userId;
        $accounting->orderSumForToday = $orderSumForToday;
        $accounting->innerSum = $innerSum;
        $accounting->taxSum = $taxSum;
        $accounting->terminalSum = $terminalSum;
        $accounting->balanceSum = $balanceSum;
        $accounting->balance_at_all = $lastBalance + $balanceSum;
        $accounting->save();

        return redirect()->action('HomeController@index');
    }
}
