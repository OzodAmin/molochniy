<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Accounting;
use App\Model\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Client;

class AccountingController extends Controller
{
    private $client;

    public function __construct()
    {
        $this->client = Client::find(1);
    }

    public function store(Request $request)
    {
        $userId = Auth::user()->id;

        $lastAccounting = Accounting::where('user_id', '=', $userId)->orderBy('created_at', 'desc')->take(1)->first();
        if (empty($lastAccounting))
            $lastBalance = 0;
        else
            $lastBalance = $lastAccounting->balance_at_all;

        $innerSum = $request->input('vnutrenniy');
        $orderSumForToday = $request->input('orderSum');
        $taxSum = $request->input('inkasasiya');
        $terminalSum = $request->input('terminal');
        $balanceSum = $orderSumForToday - $innerSum - $taxSum - $terminalSum;

        $accounting = new Accounting();
        $accounting->user_id = $userId;
        $accounting->orderSumForToday = $orderSumForToday;
        $accounting->innerSum = $innerSum;
        $accounting->taxSum = $taxSum;
        $accounting->terminalSum = $terminalSum;
        $accounting->balanceSum = $balanceSum;
        $accounting->balance_at_all = $lastBalance + $balanceSum;
        $accounting->save();

        return response()->json("", 200);
    }

    public function isAccounting()
    {
        $userId = Auth::user()->id;
        $accounting = Accounting::whereDate('created_at', '=', Carbon::today()->toDateString())->where('user_id', $userId)->first();
        if (empty($accounting))
            return response()->json("", 200);
        else
            return response()->json("", 404);
    }

    public function isOrder()
    {
        $userId = Auth::user()->id;
        $date = Carbon::today()->toDateString();
        $order = Order::whereDate('created_at', '=', $date)->where('user_id', $userId)->first();
        if (empty($order))
            return response()->json('', 404);
        else
            return response()->json($order->total_price, 200);
    }
}
