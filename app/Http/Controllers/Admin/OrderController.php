<?php

namespace App\Http\Controllers\Admin;

use App\Model\Order;
use App\Model\Product;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        if ($request->order_id > 0) {
            $orders = Order::join('users', 'orders.user_id', '=', 'users.id')->
            join('order_product', 'order_product.order_id', '=', 'orders.id')->
            join('product', 'order_product.product_id', '=', 'product.id')->
            where('orders.id', '=', $request->order_id)->
            orderBy('users.id', 'ASC')->
            select(['product.title', 'order_product.quantity', 'order_product.price']);

            return DataTables::of($orders)->make(true);
        }
        return null;
    }

    public function ordersView(Request $request)
    {
        if ($request->has('orderId')) {
            $order = Order::find($request->orderId);
            if (isset($order)) {
                $order->delete();
                Session::flash('success', 'Order deleted successfully');
            }
        }
        if ($request->has('dateFilter'))
            $date = $request->dateFilter;
        else
            $date = Carbon::today()->toDateString();

        $orders = Order::whereDate('orders.created_at', '=', $date)->orderBy('created_at', 'DESC')->get();

        return view('admin.order.index', compact('orders', 'request'));
    }

    public function ordersAllList(Request $request)
    {

        $orders = Order::join('users', 'orders.user_id', '=', 'users.id')->
        join('order_product', 'order_product.order_id', '=', 'orders.id')->
        join('product', 'order_product.product_id', '=', 'product.id')->
        whereDate('orders.created_at', $request->orderDate)->
        orderBy('users.id', 'ASC')->
        select(['orders.created_at', 'users.name', 'product.title', 'order_product.quantity', 'order_product.price']);

        return DataTables::of($orders)->make(true);

    }

    public function ordersAllView()
    {
        return view('admin.order.AllOrders');
    }

    public function ordersReportView()
    {
        $products = Product::get();
        $productsArray = [];
        foreach ($products as $item) {
            $productsArray[$item->id] = $item->title;
        }

        $uers = User::get();
        $uersArray = [];
        foreach ($uers as $item) {
            $uersArray[$item->id] = $item->name;
        }

        return view('admin.order.report', compact('productsArray', 'uersArray'));
    }

    public function ordersReport(Request $request)
    {

        $orders = Order::whereBetween('created_at', [$request->start_date, $request->end_date])->get();

        if ($request->has('products') && $request->products !== null) {
            $orders->productsPivotTable->whereIn('product.id', $request->products);
        }


        $products = Product::get();
        $productsArray = [];
        foreach ($products as $item) {
            $productsArray[$item->id] = $item->title;
        }

        $uers = User::get();
        $uersArray = [];
        foreach ($uers as $item) {
            $uersArray[$item->id] = $item->name;
        }

        return view('admin.order.report', compact('productsArray', 'uersArray', 'orders'));
    }
}
