<?php

namespace App\Http\Controllers\Admin;

use App\Model\Locations;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LocationsController extends Controller
{
    public function store(Request $request)
    {
        if ($request->has('address') && $request->address !== null && $request->has('name') && $request->name !== null) {
            $lat = $request->lat;
            $lng = $request->lng;
            $description = $request->address;
            $name = $request->name;

            Locations::create([
                'lat' => $lat,
                'lng' => $lng,
                'name' => $name,
                'address' => $description,
            ]);
            return response()->json("", 200);
        } else
            return response()->json("", 404);
    }

    public function list()
    {
        $locations = Locations::get();

        $indexed = array_map('array_values', $locations->toArray());

        $locationsEncoded = json_encode($indexed);
        return view('admin.location.list', compact('locationsEncoded'));
    }

    public function update(Request $request)
    {
        if ($request->has('delete') && $request->delete == 1) {
            Locations::destroy($request->id);
            return response()->json("", 200);
        } else {
            if ($request->has('address') && $request->address !== null && $request->has('name') && $request->name !== null) {
                $location = Locations::find($request->id);
                $location->fill($request->input())->save();
                return response()->json("", 200);
        } else
            return response()->json("", 404);
        }
    }
}