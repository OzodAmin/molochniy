<?php

namespace App\Http\Controllers\Admin;

use App\Model\System;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SystemController extends Controller
{
    public function index()
    {
        $system = System::find(1);

        return view('admin.system.index',compact('system'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $system = System::find(1);

        $system->systemBegin = $request->systemBegin;
        $system->systemEnd = $request->systemEnd;

        $system->save();

        return redirect()->route('system')->with('success','System configuration updated successfully');
    }

    public function destroy($id)
    {
        //
    }
}
