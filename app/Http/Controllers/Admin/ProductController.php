<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::orderBy('order')->get();
        return view('admin.product.index', compact('products'));
    }

    public function create()
    {
        $categories = Category::get();
        $categoriesArray = [];
        foreach ($categories as $item) {
            $categoriesArray[$item->id] = $item->name;
        }
        return view('admin.product.create', compact('categoriesArray'));
    }

    public function store(Request $request)
    {
        $this->validate($request, Product::$rules);

        //get highest order in product table
        $lastOrder = 1;
        $lastProductOrder = Product::orderBy('order', 'DESC')->take(1)->first();
        if (!empty($lastProductOrder))
            $lastOrder = $lastProductOrder->order + 1;

        $product = new Product();
        $product->title = $request->input('title');
        $product->category_id = $request->input('category_id');
        $product->price = $request->input('price');
        $product->quantity = 0;
        $product->order = $lastOrder;
        $product->save();

        return redirect()->route('products.index')->with('success', 'Product created successfully');
    }

    public function show($id)
    {
        return redirect()->route('products.index');
    }

    public function edit($id)
    {
        $product = Product::find($id);

        $categories = Category::get();
        $categoriesArray = [];
        foreach ($categories as $item) {
            $categoriesArray[$item->id] = $item->name;
        }

        return view('admin.product.edit', compact('product', 'categoriesArray'));
    }

    public function updateQuantity(Request $request)
    {
        $product = Product::find($request->id);
        $product->quantity = (int)filter_var($request->quantity, FILTER_SANITIZE_NUMBER_INT);
        $product->save();
        echo "";
    }

    public function updateOrder(Request $request)
    {
        $max = Product::max('order');
        $min = Product::min('order');

        if ($request['oldOrder'] == $min) {
            DB::table('product')
                ->where('order', '<=', $request['newOrder'])
                ->decrement('order');
        } else if ($request['oldOrder'] == $max) {
            DB::table('product')
                ->where('order', '>=', $request['newOrder'])
                ->increment('order');
        } else if ($request['newOrder'] > $request['oldOrder']) {
            DB::table('product')
                ->where('order', '>', $request['oldOrder'])
                ->where('order', '<=', $request['newOrder'])
                ->decrement('order');
        } else if ($request['newOrder'] < $request['oldOrder']) {
            DB::table('product')
                ->where('order', '>=', $request['newOrder'])
                ->where('order', '<', $request['oldOrder'])
                ->increment('order');
        }
        Product::where('id', $request['productId'])
            ->update(['order' => $request['newOrder']]);
        return redirect()->route('products.index')->with('success', 'Product updated successfully');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, Product::$rules);

        //Find the category and update its details
        $product = Product::find($id);
        $product->title = $request->input('title');
        $product->category_id = $request->input('category_id');
        $product->price = $request->input('price');
        $product->save();
        return redirect()->route('products.index')->with('success', 'Product updated successfully');
    }

    public function destroy($id)
    {
        Product::destroy($id);

        return redirect()->route('products.index')->with('success', 'Product deleted successfully');
    }
}