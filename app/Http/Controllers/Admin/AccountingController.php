<?php

namespace App\Http\Controllers\Admin;

use App\Model\Accounting;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class AccountingController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('userId') && $request->userId !== null) {
            $users = User::where('id', $request->userId)->paginate(10);
        }else{
            $users = User::orderBy('id')->paginate(10);
        }
        $uersSelect = User::get();
        $usersArray = [];
        foreach ($uersSelect as $item) {
            $usersArray[$item->id] = $item->name;
        }

        return view('admin.accounting.index', compact('users', 'usersArray'));
    }

    public function create(Request $request)
    {
        if ($request->id > 0) {
            $userAccounts = Accounting::where('user_id', '=', $request->id)->orderBy('created_at', 'DESC')->get();

            return DataTables::of($userAccounts)->make(true);
        }
        return null;
    }

    public function store(Request $request)
    {
        $accounting = new Accounting();
        $accounting->user_id = $request->input('user_id');
        $accounting->field_1 = $request->input('field_1');
        $accounting->field_2 = $request->input('field_2');
        $accounting->field_3 = $request->input('field_3');
        $accounting->field_4 = $request->input('field_4');
        $accounting->field_5 = $request->input('field_5');
        $accounting->save();

        return redirect()->route('accounting.show', ['id' => $request->input('user_id')])->with('success', 'Accounting created successfully');
    }

    public function show($id)
    {
        $user = User::find($id);
        return view('admin.accounting.show', compact('user'));
    }

    public function edit($id)
    {
        return view('admin.accounting.accountingByDay');
    }

    public function accountingByDay(Request $request)
    {
        $accounts = Accounting::join('users', 'accounting.user_id', '=', 'users.id')
            ->whereDate('accounting.created_at', '=', $request->orderDate)
            ->orderBy('accounting.balance_at_all', 'DESC')
            ->select(['accounting.*', 'users.name']);

        return DataTables::of($accounts)->make(true);
    }

    public function destroy($id)
    {
        dd($id);
    }
}
