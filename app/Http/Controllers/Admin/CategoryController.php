<?php

namespace App\Http\Controllers\Admin;

use App\Model\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::orderBy('id')->paginate(10);
        return view('admin.category.index', compact('categories'));
    }

    public function create(){return view('admin.category.create');}


    public function store(Request $request)
    {
        $this->validate($request, Category::$rules);
        //create the category role
        $category = new Category();
        $category->name = $request->input('name');
        $category->save();

        return redirect()->route('categories.index')->with('success', 'Category created successfully');
    }

    public function show($id){return redirect()->route('categories.index');}

    public function edit($id)
    {
        $category = Category::find($id);
        return view('admin.category.edit',compact('category'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, Category::$rules);

        //Find the category and update its details
        $category = Category::find($id);
        $category->name = $request->input('name');
        $category->save();
        return redirect()->route('categories.index')->with('success','Category updated successfully');
    }

    public function destroy($id)
    {
        Category::destroy($id);

        return redirect()->route('categories.index')->with('success','Category deleted successfully');
    }
}
