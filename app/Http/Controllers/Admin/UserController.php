<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('userId') && $request->userId !== null) {
            $users = User::where('id', $request->userId)->paginate(10);
        }else{
            $users = User::orderBy('id')->paginate(10);
        }
        $uersSelect = User::get();
        $usersArray = [];
        foreach ($uersSelect as $item) {
            $usersArray[$item->id] = $item->name;
        }

        return view('admin.user.index', compact('users', 'usersArray'));
    }

    public function create()
    {
        return view('admin.user.create');
    }


    public function store(Request $request)
    {
        $this->validate($request, User::$rules);

        $user = new User();
        $user->name = $request->input('name');
        $user->username = $request->input('username');
        $user->password = Hash::make($request->input('password'));
        $user->save();

        return redirect()->route('users.index')->with('success', 'User created successfully');
    }

    public function show($id)
    {
        return redirect()->route('users.index');
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.user.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if ($request->input('name') !== null){
            $user->name = $request->input('name');
            $user->username = $request->input('username');
        }elseif ($request->input('password') !== null){
            $this->validate($request, User::$rules_password);
            $user->password = Hash::make($request->input('password'));
        }else
            return redirect()->route('users.index');

        $user->save();
        return redirect()->route('users.index')->with('success', 'User updated successfully');
    }

    public function destroy($id)
    {
        User::destroy($id);

        return redirect()->route('users.index')->with('success', 'User deleted successfully');
    }
}
