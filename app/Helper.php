<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Facades\Config;


class Helper
{
    public $isTime;
    public function __construct()
    {
        $beginTime = Carbon::parse(Config::get('custom.systemBegin'));
        $endTime = Carbon::parse(Config::get('custom.systemEnd'));

        if ($beginTime->gt(Carbon::now()) || $endTime->lt(Carbon::now())) {
            $this->isTime = true;
        }
        $this->isTime = false;
    }
}