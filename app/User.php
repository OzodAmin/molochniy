<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, EntrustUserTrait;

    protected $fillable = ['name', 'email', 'password', 'username'];

    protected $hidden = ['password', 'remember_token',];

    public static $rules = [
        'name' => ['required', 'string', 'max:255'],
        'username' => ['required', 'string', 'max:255', 'unique:users'],
    ];
    public static $rules_password = ['password' => ['required', 'string', 'min:8', 'confirmed'],];

    public function findForPassport($username)
    {
        return $this->where('username', $username)->first();
    }

    public function accounting()
    {
        return $this->hasMany('App\Model\Accounting');
    }
}
